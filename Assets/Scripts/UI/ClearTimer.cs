using System;
using UnityEngine;
using UnityEngine.UI;

public class ClearTimer : MonoBehaviour {
    [SerializeField] Text timeText;
    [SerializeField] VoidEventChannel levelStartedEventChannel;
    [SerializeField] VoidEventChannel levelCleareddEventChannel;
    [SerializeField] StringEventChannel clearTimeEventChannel;
    bool stop = true;
    float clearTime;
    private void OnEnable() {
        levelStartedEventChannel.AddListener(LevelStart);
        levelCleareddEventChannel.AddListener(LevelClear);
    }
    private void OnDisable() {
        levelStartedEventChannel.RemoveListener(LevelStart);
        levelCleareddEventChannel.RemoveListener(LevelClear);
    }
    private void FixedUpdate() {
        if (stop) return;
        clearTime += Time.fixedDeltaTime;
        //格式化时间
        timeText.text = System.TimeSpan.FromSeconds(clearTime).ToString(@"mm\:ss\:ff");
    }
    private void LevelStart() {
        stop = false;
    }
    private void LevelClear() {
        stop = true;
        clearTimeEventChannel.Broadcast(timeText.text);
    }

}
