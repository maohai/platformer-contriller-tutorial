using UnityEngine;

public class ReadyScreen : MonoBehaviour {
    [SerializeField] VoidEventChannel levelStartedEventChannel;
    [SerializeField] AudioClip startVoice;

    void LevelStart() {
        levelStartedEventChannel.Broadcast();
        GetComponent<Canvas>().enabled = false;
        GetComponent<Animator>().enabled = false;
    }
    void PlayStartVoice() {
        SoundEffectsPlayer.AudioSource.PlayOneShot(startVoice);
    }

}
