using UnityEngine.SceneManagement;

public class SceneLoader {
    public static void ReloadScene() {//重新加载此场景
        int sceneIndex = SceneManager.GetActiveScene().buildIndex;
        SceneManager.LoadScene(sceneIndex);
    }
    public static void LoadNextScene() {
        int sceneIndex = SceneManager.GetActiveScene().buildIndex+1;
        if (sceneIndex >= SceneManager.sceneCount) {
            ReloadScene();
            return;
        }
        SceneManager.LoadScene(sceneIndex);
    }
    public static void QuitGame() {
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;//退出编辑器
#else
        Application.Quit();
#endif
    }
}
