using System;
using UnityEngine;
public class PlayerController : MonoBehaviour {
    PlayerGroundedDetector groundedDetector;
    PlayerInput input;
    Rigidbody rigidBody;
    public AudioSource VoicePlayer { get; private set; }//获取音源组件
    [SerializeField] VoidEventChannel levelClearedEventChannel;

    private void Awake() {
        groundedDetector = GetComponentInChildren<PlayerGroundedDetector>();
        input = GetComponent<PlayerInput>();
        rigidBody = GetComponent<Rigidbody>();
        VoicePlayer = GetComponentInChildren<AudioSource>();
    }

    private void Start() {
        input.EnableGameplayInputs();//启用表
    }
    private void OnEnable() {
        levelClearedEventChannel.AddListener(OnLevelCleared);
    }
    private void OnDisable() {
        levelClearedEventChannel.RemoveListener(OnLevelCleared);
    }

    private void OnLevelCleared() {
        Victory = true;
    }
    public void OnDefeated() {
        input.DisableGameplayInputs();

        rigidBody.velocity = Vector3.zero;//速度为0
        rigidBody.useGravity = false;//失去重力
        rigidBody.detectCollisions = false;//关闭刚体的碰撞检测,防止玩家与尖刺再次碰撞
        //切换为死亡状态
        GetComponent<StateMachine>().SwitchState(typeof(PlayerState_Defeated));
    }

    public void SetVelocity(Vector3 velocity) => rigidBody.velocity = velocity; //设置刚体的速度
    public void SetVelocityX(float velocityX) => rigidBody.velocity = new Vector3(velocityX, rigidBody.velocity.y);//设置刚体X轴的值
    public void SetVelocityY(float velocityY) => rigidBody.velocity = new Vector3(rigidBody.velocity.x, velocityY);//设置刚体Y轴上的值
    public void Move(float speed) {//移动函数  传入速度,然后设置(速度*方向上的轴的值)即可
        if (input.Move) {//如果是移动状态
            transform.localScale = new Vector3(input.AxisX, 1, 1);//镜像翻转
        }
        SetVelocityX(speed * input.AxisX);
    }
    public float MoveSpeed => Mathf.Abs(rigidBody.velocity.x); //获取玩家速度
    public bool IsGrounded => groundedDetector.IsGrounded;//检测是否在地面上
    public bool IsFalling => rigidBody.velocity.y < 0f && !IsGrounded;//当有一个向下的力,而且没在地面上的时候
    public bool CanAirJump { get; set; }//默认为false //检测能否空中跳跃,默认为true
    public void SetUseGravity(bool value) => rigidBody.useGravity = value;//设置重力
    public bool Victory { get; private set; }//设置胜利
}
