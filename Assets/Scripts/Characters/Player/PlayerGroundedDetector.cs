using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerGroundedDetector : MonoBehaviour {
    [SerializeField] float detectionRadius = 0.1f;
    Collider[] colliders = new Collider[1];//因为只需要检测地面,所以就碰撞一个就足够了
    [SerializeField] LayerMask groundlayer;

    //碰撞器或者触发器重叠的时候
    //OverlapSphereNonAlloc() 投射虚拟球体
    //如果投射出的球体和某个碰撞体产生了重叠,也就是碰撞
    //(球的圆心,球的半径,储存这个球所重叠的所有碰撞器数组,检测的层级,可选是否需要检测触发器)
    //返回一个碰撞的物体的数量
    //Physics.OverlapSphere();
    public bool IsGrounded => Physics.OverlapSphereNonAlloc(transform.position, detectionRadius, colliders, groundlayer) != 0;//不会有内存分配
     
    //用线画出检测的球的大小,便于观察
    private void OnDrawGizmos() {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, detectionRadius);
    }

}
