using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StarGem : MonoBehaviour
{
    [SerializeField] float resetTime = 3.0f;
    [SerializeField] AudioClip puckUpSFX;//拾取音效
    [SerializeField] ParticleSystem pickUpVFX;//拾取特效
    new Collider collider;
    MeshRenderer meshRenderer;
    WaitForSeconds waitResetTime;
    private void Awake() {
        collider = GetComponent<Collider>();
        meshRenderer = GetComponentInChildren<MeshRenderer>();
        waitResetTime = new WaitForSeconds(resetTime);
    }
    private void OnTriggerEnter(Collider other) {
        if(other.TryGetComponent<PlayerController>(out PlayerController player)) {
            player.CanAirJump = true;
            collider.enabled = false;
            meshRenderer.enabled = false;

            SoundEffectsPlayer.AudioSource.PlayOneShot(puckUpSFX);//播放一次
            Instantiate(pickUpVFX, other.transform.position, Quaternion.identity);//生成特效
            StartCoroutine(nameof(ResetCoroutine));
        }
    }
    void ResetStar() {
        collider.enabled = true;
        meshRenderer.enabled = true;
    }
    IEnumerator ResetCoroutine() {
        yield return waitResetTime;
        ResetStar();
    }
}
