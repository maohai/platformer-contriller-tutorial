using System;
using UnityEngine;

public class BlackBall : MonoBehaviour
{
    [SerializeField] VoidEventChannel gateTriggerEventChannel;
    [SerializeField] float lifeTime = 10f;
    private void OnEnable() {
        gateTriggerEventChannel.AddListener(OnGateTriggered);
    }
    private void OnDisable() {
        gateTriggerEventChannel.RemoveListener(OnGateTriggered);
    }

    private void OnGateTriggered() {
        Destroy(gameObject, lifeTime);
    }

    private void OnCollisionEnter(Collision collision) {
        if (collision.gameObject.TryGetComponent(out PlayerController player))
            player.OnDefeated();
    }
}
