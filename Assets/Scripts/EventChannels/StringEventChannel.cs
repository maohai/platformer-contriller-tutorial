using UnityEngine;

[CreateAssetMenu(menuName = "Data/EventChannels/StringEventChannel", fileName = "StringEventChannel_")]
public class StringEventChannel : OneParameterEventChanel<string>
{
    
}
