using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StateMachine : MonoBehaviour {
    IState currentState;
    protected Dictionary<System.Type, IState> stateTable;//<类型类,接口>
    private void Update() {
        currentState.LogicUpdate();//逻辑状态更新
    }
    private void FixedUpdate() {
        currentState.PhysicUpdate();//物理状态更新 
    }
    protected void SwitchOn(IState newState) { //当前状态的启动
        currentState = newState;//设置为新状态
        currentState.Enter();//调用enter来启用
    }
    public void SwitchState(IState newState) {//共有状态,切换当前方法
        currentState.Exit();//退出当前状态
        SwitchOn(newState);//掐换为新状态
    }
    public void SwitchState(System.Type newStateType) {
        SwitchState(stateTable[newStateType]);//重载
    }
}
