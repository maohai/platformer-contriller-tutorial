using UnityEngine;
[CreateAssetMenu(menuName = "Data/StateMachine/PlayerState/JumpUp", fileName = "PlayerState_JumpUp")]
public class PlayerState_JumpUp : PlayerState {
    [SerializeField] float jumpForce = 7f;
    [SerializeField] float speedMove = 5f;
    [SerializeField] ParticleSystem jumpVFX;//跳跃特效
    [SerializeField] AudioClip jumpSFX;
    public override void Enter() {
        base.Enter();
        input.HasJumpInputBuffer = false;//关闭缓冲跳跃

        player.SetVelocityY(jumpForce);
        Instantiate(jumpVFX, player.transform.position, Quaternion.identity);
        player.VoicePlayer.PlayOneShot(jumpSFX);

    }
    public override void LogicUpdate() {
        if (input.StopJump || player.IsFalling) {
            stateMachine.SwitchState(typeof(PlayerState_Fall));//切换为掉落
        }
    }
    public override void PhysicUpdate() {
        player.Move(speedMove);
    }
}
