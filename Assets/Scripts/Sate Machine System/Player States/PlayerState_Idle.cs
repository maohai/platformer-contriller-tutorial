using UnityEngine;
[CreateAssetMenu(menuName ="Data/StateMachine/PlayerState/Idle",fileName ="PlayerState_Idle")]

public class PlayerState_Idle : PlayerState
{
    [SerializeField] float deceleration = 5f;
    public override void Enter() {
        base.Enter();
        currentSpeed = player.MoveSpeed;
    }
    public override void LogicUpdate() {
        if (input.Move)
            stateMachine.SwitchState(typeof(PlayerState_Run));//切换为跑步动作
        if (input.Jump)
            stateMachine.SwitchState(typeof(PlayerState_JumpUp));//切换为跳跃
        if (!player.IsGrounded)
            stateMachine.SwitchState(typeof(PlayerState_Fall));//切换为掉落
        currentSpeed = Mathf.MoveTowards(currentSpeed, 0, deceleration * Time.deltaTime);
    }
    public override void PhysicUpdate() {
        //因为停止移动的时候没有任何输入,但玩家移动是有左右两个方向,所以得乘上方向
        player.SetVelocityX(currentSpeed * player.transform.localScale.x);
    }
}
