using UnityEngine;
[CreateAssetMenu(menuName = "Data/StateMachine/PlayerState/AirJump", fileName = "PlayerState_AirJump")]

public class PlayerState_AirJump : PlayerState
{
    [SerializeField] float jumpForce = 7f;
    [SerializeField] float speedMove = 5f;
    [SerializeField] ParticleSystem jumpVFX;//��Ծ��Ч
    [SerializeField] AudioClip jumpSFX;
    public override void Enter() {
        base.Enter();
        player.CanAirJump = false;

        player.SetVelocityY(jumpForce);
        Instantiate(jumpVFX, player.transform.position, Quaternion.identity);
        player.VoicePlayer.PlayOneShot(jumpSFX);
    }
    public override void LogicUpdate() {
        if (input.StopJump || player.IsFalling) {
            stateMachine.SwitchState(typeof(PlayerState_Fall));//�л�Ϊ����
        }
    }
    public override void PhysicUpdate() {
        player.Move(speedMove);
    }

}
