using UnityEngine;
[CreateAssetMenu(menuName ="Data/StateMachine/PlayerState/Victory",fileName ="PlayerState_Victory")]
public class PlayerState_Victory : PlayerState
{
    [SerializeField] AudioClip[] Voice;
    public override void Enter() {
        base.Enter();
        input.DisableGameplayInputs();//�ر�����
        //SoundEffectsPlayer.AudioSource.PlayOneShot(Voice[Random.Range(0, Voice.Length)]);
        player.VoicePlayer.PlayOneShot(Voice[Random.Range(0, Voice.Length)]);
    }
}
