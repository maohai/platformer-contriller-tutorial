using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerStateMachine:StateMachine//将挂载到玩家身上
{
    public PlayerState_Idle idleState;
    public PlayerState_Run runState;
    [SerializeField] PlayerState[] states;

    Animator animator;
    PlayerController player;
    PlayerInput input;

    private void Awake() {
        animator = GetComponentInChildren<Animator>();
        player = GetComponent<PlayerController>();
        input = GetComponent<PlayerInput>();
        stateTable = new Dictionary<System.Type, IState>(states.Length);//初始化表

        foreach (var state in states) {
            state.Initialize(animator, player, input,this);
            stateTable.Add(state.GetType(), state);//GetType()  获取类型,因为类型是唯一的
        }
    }
    private void Start() {
        SwitchOn(stateTable[typeof(PlayerState_Idle)]);
    }

}
