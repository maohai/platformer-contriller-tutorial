using UnityEngine;
[CreateAssetMenu(menuName = "Data/StateMachine/PlayerState/Land", fileName = "PlayerState_Land")]
public class PlayerState_Land : PlayerState
{
    [SerializeField] float stiffTime = 0.2f;//硬直时间
    [SerializeField] ParticleSystem LandVFX;//灰尘特效
    public override void Enter() {
        base.Enter();
        player.SetVelocity(Vector3.zero);
        Instantiate(LandVFX, player.transform.position, Quaternion.identity);
    }
    public override void LogicUpdate() {
        if (player.Victory)
            stateMachine.SwitchState(typeof(PlayerState_Victory));
        if (input.HasJumpInputBuffer||input.Jump)//当有输入缓冲时或者跳跃的时候
            stateMachine.SwitchState(typeof(PlayerState_JumpUp));
        if (StateDuration < stiffTime) return;//如果小于硬直时间,直接返回
        if (input.Move)
            stateMachine.SwitchState(typeof(PlayerState_Run));
        if (IsAnimationFinished)//当掉落完成的时候
            stateMachine.SwitchState(typeof(PlayerState_Idle));
    }
}
