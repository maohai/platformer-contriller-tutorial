using UnityEngine;
[CreateAssetMenu(menuName = "Data/StateMachine/PlayerState/Run", fileName = "PlayerState_Run")]
public class PlayerState_Run : PlayerState
{
    [SerializeField] float runSpeed = 5f;
    [SerializeField] float acceleration = 5f;
    public override void Enter() {
        base.Enter();
        currentSpeed = player.MoveSpeed;
    }
    public override void LogicUpdate() {
        if (!input.Move)
            stateMachine.SwitchState(typeof(PlayerState_Idle));//切换为闲置动作
        if (input.Jump)
            stateMachine.SwitchState(typeof(PlayerState_JumpUp));//切换为跳跃
        if (!player.IsGrounded)
            stateMachine.SwitchState(typeof(PlayerState_CoyoteTime));//切换为掉落
        //MoveTowards(当前值,目标值,每一帧所变化的值)
        currentSpeed = Mathf.MoveTowards(currentSpeed, runSpeed, acceleration * Time.deltaTime);
    }
    public override void PhysicUpdate() {
        player.Move(currentSpeed);
    }
}
