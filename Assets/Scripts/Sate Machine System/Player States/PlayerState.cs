using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerState : ScriptableObject, IState//可程序化对象类
{
    [SerializeField] string stateName;//动画名称
    [SerializeField, Range(0, 1f)] float transitionDuration = 0.1f;//交叉淡化时间,也就是融合时间
    int stateHash;//哈希id
    float stateStartTime;

    protected float currentSpeed;

    protected Animator animator;//进行 动画切换
    protected PlayerController player;
    protected PlayerInput input;
    protected PlayerStateMachine stateMachine;//玩家状态基类,施行状态切换

    protected float StateDuration => Time.time - stateStartTime;//动画现在的时间减去开始的时间
    protected bool IsAnimationFinished => StateDuration >= animator.GetCurrentAnimatorStateInfo(0).length;//判断当前动画的时间是否大于等于动画的时间长度
    public void Initialize(Animator animator, PlayerController player, PlayerInput input,PlayerStateMachine stateMachine) {//初始化组件
        this.animator = animator;
        this.player = player;
        this.input = input;
        this.stateMachine = stateMachine;
    }
    private void OnEnable() {
        stateHash = Animator.StringToHash(stateName);
    }
    public virtual void Enter() {
        //交叉淡化
        animator.CrossFade(stateHash, transitionDuration);
        stateStartTime = Time.time;
    }

    public virtual void Exit() { }

    public virtual void LogicUpdate() { }

    public virtual void PhysicUpdate() { }
}
