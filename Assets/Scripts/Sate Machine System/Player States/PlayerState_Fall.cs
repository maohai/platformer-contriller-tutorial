using UnityEngine;
[CreateAssetMenu(menuName = "Data/StateMachine/PlayerState/Fall", fileName = "PlayerState_Fall")]

public class PlayerState_Fall : PlayerState
{
    [SerializeField] AnimationCurve speedCurve;//动画曲线变量
    [SerializeField] float speedMove = 5f;
    public override void LogicUpdate() {
        if (player.IsGrounded)
            stateMachine.SwitchState(typeof(PlayerState_Land));
        if(input.Jump) {
            if (player.CanAirJump) {
                stateMachine.SwitchState(typeof(PlayerState_AirJump));//切换为空中跳跃状态
                return;
            }
            input.SetJumpInputBufferTimer();// 当玩家不能进行二段跳且处于掉落状态
        }
    }
    public override void PhysicUpdate() {
        player.Move(speedMove);
        //根据曲线来变化Y轴的值
        player.SetVelocityY(speedCurve.Evaluate(StateDuration));

    }
}
