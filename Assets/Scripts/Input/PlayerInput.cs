using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInput : MonoBehaviour
{
    PlayerInputAction playerInputActions;
    [SerializeField] float jumpInputBufferTime = 0.5f;
    WaitForSeconds waitJumpInputBufferTime;

    private void Awake() {
        playerInputActions = new PlayerInputAction();
        waitJumpInputBufferTime = new WaitForSeconds(jumpInputBufferTime);
    }
    public void EnableGameplayInputs() {
        //启用某个动作表,只需要调用他的Enable就可以了
        playerInputActions.Gameplay.Enable();
        Cursor.lockState = CursorLockMode.Locked;//锁定鼠标
    }
    private void OnEnable() {
        //当Jump按键松开的时候就会执行delegate {  HasJumpInputBuffer = false; };
        //也就是jump消失时候,canceled添加一个订阅
        playerInputActions.Gameplay.Jump.canceled += delegate {
            HasJumpInputBuffer = false;
        };
    }

    public void DisableGameplayInputs() {
        //禁用
        playerInputActions.Gameplay.Disable();
    }

    //private void OnGUI() {
    //    Rect rect = new Rect(200, 200, 200, 200);
    //    string message = "Has Jump Input Buffer: " + HasJumpInputBuffer;
    //    GUIStyle style = new GUIStyle();
    //    style.fontSize = 20;
    //    style.fontStyle = FontStyle.Bold;
    //    GUI.Label(rect, message, style);
    //}
    //WasPressedThisFrame() 就是按下Jump动作中的不管哪一个绑定的按键,都会返回一个True
    public bool Jump => playerInputActions.Gameplay.Jump.WasPressedThisFrame();
    public bool StopJump => playerInputActions.Gameplay.Jump.WasReleasedThisFrame();//同上,只要不按,就一直返回false
    Vector2 axes => playerInputActions.Gameplay.Axes.ReadValue<Vector2>();//获取一个轴上的值,(-1,1)之间
    public float AxisX => axes.x; //取得x轴上的值
    public bool Move => AxisX != 0f;//判断是否移动
    public bool HasJumpInputBuffer { get; set; }//判断是否有输入缓冲
    public void SetJumpInputBufferTimer() {
        StopCoroutine(nameof(JumpInputBufferCouroutine));
        StartCoroutine(nameof(JumpInputBufferCouroutine));
    }
    IEnumerator JumpInputBufferCouroutine() {
        HasJumpInputBuffer = true;
        yield return waitJumpInputBufferTime;
        HasJumpInputBuffer = false;
    }

}
